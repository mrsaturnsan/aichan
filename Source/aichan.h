/******************************************************************************/
/*
* @file   aichan.h
* @author Aditya Harsh
* @brief  Neural-network framework.

          Copyright (c) 2018 Aditya Harsh
          Permission is hereby granted, free of charge, to any person obtaining 
          a copy of this software and associated documentation files 
          (the "Software"), to deal in the Software without restriction, 
          including without limitation the rights to use, copy, modify, merge, 
          publish, distribute, sublicense, and/or sell copies of the Software, 
          and to permit persons to whom the Software is furnished to do so, 
          subject to the following conditions:
          The above copyright notice and this permission notice shall be 
          included in all copies or substantial portions of the Software.
          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
          MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
          NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
          BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
          ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
          CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
          SOFTWARE.
*/
/******************************************************************************/

#pragma once

#include <vector>             //std::vector
#include <initializer_list>   //std::initializer_list
#include <iostream>           //std::ostream
#include <utility>            //std::pair

namespace AIChan
{
    /**
     * @brief Matrix class to help with AI.
     * 
     */
    class Matrix
    {
        class Proxy
        {
        public:
            Proxy(Matrix& mat, size_t outer_index);
            double& operator[](size_t index);
        private:
            Matrix& mat_;
            const size_t outer_index_;
        };

        class ConstProxy
        {
        public:
            ConstProxy(const Matrix& mat, size_t outer_index);
            double operator[](size_t index) const;
        private:
            const Matrix& mat_;
            const size_t outer_index_;
        };

        friend class Proxy;
        friend class ConstProxy;

    public:
        Matrix(size_t rows, size_t columns);
        Matrix(size_t rows, size_t columns, const std::initializer_list<double>& values);
        
        /**
         * @brief Construct a new Matrix object.
         *
         * @tparam InputIt The type of range.
         * @param rows The number of rows.
         * @param columns The number of columns.
         * @param first Start of the range.
         * @param last End of the range.
         */
        template <typename InputIt>
        Matrix(size_t rows, size_t columns, InputIt first, InputIt last) : Matrix{rows, columns}
        {
            if (area_ != static_cast<size_t>(std::distance(first, last)))
                throw std::logic_error("The range does not match the dimensions of the matrix!");

            std::copy(first, last, mat_.get());
        }

        Matrix(const Matrix& other);
        Matrix(Matrix&& other) noexcept;
        Matrix& operator=(const Matrix& other);
        Matrix& operator=(Matrix&& other) noexcept;
        Matrix operator*(const Matrix& other) const;
        Matrix& operator*=(const Matrix& other);
        Matrix operator+(const Matrix& other) const;
        Matrix& operator+=(const Matrix& other);
        Matrix operator-(const Matrix& other) const;
        Matrix& operator-=(const Matrix& other);
        Proxy operator[](size_t index);
        ConstProxy operator[](size_t index) const;
        size_t Rows() const noexcept;
        size_t Columns() const noexcept;
        double Dot(const Matrix& other) const;
        void Transpose() noexcept;

        /**
         * @brief Construct a new Matrix object.
         *
         * @tparam InputIt The type of range.
         * @param first Start of the range.
         * @param last End of the range.
         */
        template <typename InputIt>
        void Fill(InputIt first, InputIt last)
        {
            if (area_ != static_cast<size_t>(std::distance(first, last)))
                throw std::logic_error("The range does not match the dimensions of the matrix!");

            std::copy(first, last, mat_.get());
        }

        friend void swap(Matrix& lhs, Matrix& rhs) noexcept;
        friend std::ostream& operator<<(std::ostream& os, const Matrix& matrix);
    private:
        size_t rows_;
        size_t columns_;
        size_t area_;
        std::unique_ptr<double[]> mat_;
    };

    /**
     * @brief Swaps two matrices.
     * 
     * @param lhs First matrix.
     * @param rhs Second matrix.
     */
    void swap(Matrix& lhs, Matrix& rhs) noexcept;

    /**
     * @brief Struct for setting up the neural network.
     * 
     */
    struct AIConfig
    {
        // allow for modifying variables individually
        AIConfig() = default;

        AIConfig(size_t input_size, size_t hidden_layers, size_t hidden_layer_size, 
                 size_t output_size, double learning_curve, size_t learning_curve_halve_rate = 0);

        size_t input_size_;
        size_t hidden_layers_;
        size_t hidden_layer_size_;
        size_t output_size_;
        double learning_curve_;
        size_t learning_curve_halve_rate_;
    };

    /**
     * @brief Gradient-descent based neural network.
     * 
     */
    class AI
    {
    public:
        // Standard constructor
        AI(const AIConfig& config);
        // Sends input into the neural network system
        void ForwardPropagate(const Matrix& input);
        // Given an expected output, the neural network trains to correct itself
        void BackwardPropagate(const Matrix& expected);
        // Gets the output of a given input
        const Matrix& GetOutput() const noexcept;
        // Gets the index of the neuron with the max activation along and with its activation
        std::pair<size_t, double> GetMaxOutput() const noexcept;
        // Gets the index of the neuron with the min activation along and with its activation
        std::pair<size_t, double> GetMinOutput() const noexcept;
        // Gets the indices of all neurons about a certain threshold
        std::vector<size_t> GetThresholdNeurons(double threshold) const;
        // Resets the current learning curve back to what was originally specified
        void ResetLearningCurve() noexcept;
        // Saves the current state of the network to a file
        void SaveState(const std::string& file_name) const;
        // Loads the file state into the network
        void LoadState(const std::string& file_name);
    private:
        // Matrix data
        std::vector<Matrix> activations_;
        std::vector<Matrix> weights_;
        std::vector<Matrix> biases_;

        // user provided configuration
        const AIConfig config_;

        // used for keeping track of when to cut the learning rate
        double learning_curve_;
        size_t current_iterations_;

        void AdjustLearningCurve();
    };
}
