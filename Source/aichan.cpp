/******************************************************************************/
/*
* @file   aichan.cpp
* @author Aditya Harsh
* @brief  Neural-network framework.

          Copyright (c) 2018 Aditya Harsh
          Permission is hereby granted, free of charge, to any person obtaining 
          a copy of this software and associated documentation files 
          (the "Software"), to deal in the Software without restriction, 
          including without limitation the rights to use, copy, modify, merge, 
          publish, distribute, sublicense, and/or sell copies of the Software, 
          and to permit persons to whom the Software is furnished to do so, 
          subject to the following conditions:
          The above copyright notice and this permission notice shall be 
          included in all copies or substantial portions of the Software.
          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
          MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
          NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
          BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
          ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
          CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
          SOFTWARE.
*/
/******************************************************************************/

#include "aichan.h"

#include <random>               //std::random_device, std::mt19937, std::uniform_real_distribution
#include <fstream>              //std::ofstream, std::ifstream
#include <sstream>              //std::string, std::isstringstream
#include <limits>               //std::numeric_limits::epsilon

namespace AIChan
{   
    namespace
    {
        // RNG
        std::random_device rd;
        std::mt19937 gen{rd()};
        std::uniform_real_distribution<> dis{-1.0, 1.0};

        /**
         * @brief Fills in a matrix with random values.
         * 
         * @param matrix The matrix being filled.
         */
        void Randomize(Matrix& matrix) noexcept
        {
            for (size_t x = 0; x < matrix.Rows(); ++x)
                for (size_t y = 0; y < matrix.Columns(); ++y)
                    matrix[x][y] = dis(gen);
        }
    }

    /**
     * @brief Construct a new Matrix:: Proxy:: Proxy object.
     * 
     * @param mat The matrix being examined.
     * @param outer_index The row.
     */
    Matrix::Proxy::Proxy(Matrix& mat, size_t outer_index) : mat_{mat}, outer_index_{outer_index}
    {
        if (outer_index >= mat_.rows_)
            throw std::out_of_range("Attempting to access invalid index!");
    }

    /**
     * @brief Non-const subscript overload.
     * 
     * @param index The column.
     * @return double& 
     */
    double& Matrix::Proxy::operator[](size_t index)
    {
        if (index >= mat_.columns_)
            throw std::out_of_range("Attempting to access invalid index!");

        return mat_.mat_[index + outer_index_ * mat_.columns_];
    }

    /**
     * @brief Construct a new Matrix:: Proxy:: Proxy object.
     * 
     * @param mat The matrix being examined.
     * @param outer_index The row.
     */
    Matrix::ConstProxy::ConstProxy(const Matrix& mat, size_t outer_index) : mat_{mat}, outer_index_{outer_index}
    {
        if (outer_index >= mat_.rows_)
            throw std::out_of_range("Attempting to access invalid index!");
    }

    /**
     * @brief Non-const subscript overload.
     * 
     * @param index The column.
     * @return double& 
     */
    double Matrix::ConstProxy::operator[](size_t index) const
    {
        if (index >= mat_.columns_)
            throw std::out_of_range("Attempting to access invalid index!");

        return mat_.mat_[index + outer_index_ * mat_.columns_];
    }

    /**
     * @brief Construct a new Matrix:: Matrix object.
     * 
     * @param rows Number of rows the matrix has.
     * @param columns Number of columns the matrix has.
     */
    Matrix::Matrix(size_t rows, size_t columns) : rows_{rows}, columns_{columns}, area_{rows * columns}
    {
        if (area_ == 0)
            throw std::logic_error("Need at least 1 row and column!");

        mat_ = std::make_unique<double[]>(area_);

        std::memset(mat_.get(), 0, area_ * sizeof(double));
    }

    /**
     * @brief Construct a new Matrix. Allows for filling in with values using an initializer list.
     * 
     * @param rows Number of rows the matrix has.
     * @param columns Number of columns the matrix has.
     * @param values The values to insert into the matrix.
     */
    Matrix::Matrix(size_t rows, size_t columns, const std::initializer_list<double>& values) : Matrix{rows, columns}
    {
        if (area_ != values.size())
                throw std::logic_error("The range does not match the dimensions of the matrix!");

        std::copy(std::cbegin(values), std::cend(values), mat_.get());
    }

    /**
     * @brief Construct a new Matrix:: Matrix object.
     * 
     * @param other The matrix to copy from.
     */
    Matrix::Matrix(const Matrix& other) : Matrix{other.rows_, other.columns_}
    {
        std::memcpy(mat_.get(), other.mat_.get(), area_ * sizeof(double));
    }

    /**
     * @brief Move constructor for a new Matrix object.
     * 
     * @param other The matrix to move from.
     */
    Matrix::Matrix(Matrix&& other) noexcept : rows_{std::exchange(other.rows_, 0)}, columns_{std::exchange(other.columns_, 0)}, area_{std::exchange(other.area_, 0)}, mat_{std::move(other.mat_)}
    {

    }

    /**
     * @brief Sets two matrices equal to one another.
     * 
     * @param other The matrix being set from.
     * @return Matrix& 
     */
    Matrix& Matrix::operator=(const Matrix& other)
    {
        if (area_ >= other.area_)
        {
            rows_ = other.rows_;
            columns_ = other.columns_;
            area_ = other.area_;
            std::memcpy(mat_.get(), other.mat_.get(), area_ * sizeof(double));
        }
        else // not big enough
        {
            Matrix tmp {other};
            swap(*this, tmp);
        }
        
        return *this;
    }

    /**
     * @brief Move assignment.
     * 
     * @param other The matrix being set from.
     * @return Matrix& 
     */
    Matrix& Matrix::operator=(Matrix&& other) noexcept
    {
        rows_ = std::exchange(other.rows_, 0);
        columns_ = std::exchange(other.columns_, 0);
        area_ = std::exchange(other.area_, 0);
        mat_ = std::move(other.mat_);
        return *this;
    }

    /**
     * @brief Multiplies with another matrix, and returns the output.
     * 
     * @param other The matrix being multiplied with.
     * @return Matrix 
     */
    Matrix Matrix::operator*(const Matrix& other) const
    {
        Matrix new_mat{*this}; 
        new_mat *= other;
        return new_mat;
    }

    /**
     * @brief Multiplies with another matrix, and sets matrix equal to the output.
     * 
     * @param other The matrix being multiplied with.
     * @return Matrix& 
     */
    Matrix& Matrix::operator*=(const Matrix& other)
    {
        if (columns_ != other.rows_)
            throw std::logic_error("Invalid matrix multiplication! Check dimensions!");

        // update the area
        area_ = rows_ * other.columns_;
            
        std::vector<double> temp;
        temp.assign(area_, 0);

        if (other.columns_ == 1)
        {
            for (size_t i = 0; i < rows_; ++i)
                for (size_t j = 0; j < columns_; ++j)
                    temp[i * other.columns_] += mat_[j + i * columns_] * other.mat_[j * other.columns_];
        }
        else
        {
            for (size_t i = 0; i < rows_; ++i)
                for (size_t j = 0; j < other.columns_; ++j)
                    for (size_t k = 0; k < columns_; ++k)
                        temp[j + i * other.columns_] += mat_[k + i * columns_] * other.mat_[j + k * other.columns_];
        }

        // update columns and copy data
        columns_ = other.columns_;
        std::memcpy(mat_.get(), temp.data(), area_ * sizeof(double));
        
        return *this;
    }

    /**
     * @brief Adds with another matrix, and returns the output.
     * 
     * @param other The matrix being added with.
     * @return Matrix 
     */
    Matrix Matrix::operator+(const Matrix& other) const
    {
        Matrix new_mat{*this};
        new_mat += other;
        return new_mat;
    }

    /**
     * @brief Adds with another matrix, and sets matrix equal to the output.
     * 
     * @param other The matrix being added with.
     * @return Matrix& 
     */
    Matrix& Matrix::operator+=(const Matrix& other)
    {
        if (columns_ != other.columns_ || rows_ != other.rows_)
            throw std::logic_error("Invalid matrix addition! Check dimensions!");

        for (size_t i = 0; i < area_; ++i)
            mat_[i] += other.mat_[i];

        return *this;
    }

    /**
     * @brief Subtracts with another matrix, and returns the output.
     * 
     * @param other The matrix being subtracted with.
     * @return Matrix 
     */
    Matrix Matrix::operator-(const Matrix& other) const
    {
        Matrix new_mat{*this};
        new_mat -= other;
        return new_mat;
    }

    /**
     * @brief Subtracts with another matrix, and sets matrix equal to the output.
     * 
     * @param other The matrix being subtracted with.
     * @return Matrix& 
     */
    Matrix& Matrix::operator-=(const Matrix& other)
    {
        if (columns_ != other.columns_ || rows_ != other.rows_)
            throw std::logic_error("Invalid matrix subtraction! Check dimensions!");

        for (size_t i = 0; i < area_; ++i)
            mat_[i] -= other.mat_[i];

        return *this;
    }

    /**
     * @brief Subscript overload.
     * 
     * @param index The row.
     * @return Matrix::Proxy 
     */
    Matrix::Proxy Matrix::operator[](size_t index)
    {
        return Proxy{*this, index};
    }

    /**
     * @brief Const subscript overload.
     * 
     * @param index The row.
     * @return const Matrix::Proxy 
     */
    Matrix::ConstProxy Matrix::operator[](size_t index) const
    {
        return ConstProxy{*this, index};
    }

    /**
     * @brief Returns the number of rows.
     * 
     * @return size_t 
     */
    size_t Matrix::Rows() const noexcept
    {
        return rows_;
    }

    /**
     * @brief Returns the number of columns.
     * 
     * @return size_t 
     */
    size_t Matrix::Columns() const noexcept
    {
        return columns_;
    }

    /**
     * @brief Calculates the dot product of two 1-column matrices (vectors).
     * 
     * @param other The matrix being dotted with.
     * @return double 
     */
    double Matrix::Dot(const Matrix& other) const
    {
        if (columns_ != 1 || other.columns_ != 1 || rows_ != other.rows_)
            throw std::logic_error("Invalid dot product operation. Check matrix dimensions!");

        double dotp = 0;

        for (size_t i = 0; i < rows_; ++i)
            dotp += mat_[i] * other.mat_[i];

        return dotp;
    }

    /**
     * @brief Transposes a matrix.
     * 
     */
    void Matrix::Transpose() noexcept
    {
        std::vector<double> temp;
        temp.reserve(area_);
        
        for (size_t i = 0; i < rows_; ++i)
            for (size_t j = 0; j < columns_; ++j)
                temp[i + j * rows_] = mat_[j + i * columns_];

        // copy over
        std::memcpy(mat_.get(), temp.data(), area_ * sizeof(double));
        std::swap(rows_, columns_);
    }

    /**
     * @brief Insertion operator to help with debugging.
     * 
     * @param os Output stream.
     * @param matrix Matrix being printed out.
     * @return std::ostream& 
     */
    std::ostream& operator<<(std::ostream& os, const Matrix& matrix)
    {  
        for (size_t i = 0; i < matrix.rows_; ++i)
        {
            for (size_t j = 0; j < matrix.columns_; ++j)
            {
                os << matrix.mat_[j + i * matrix.columns_];

                if (j < matrix.columns_ - 1)
                    os << ' ';
            }
                  
            if (i < matrix.rows_ - 1)
                os << '\n';
        }

        return os;  
    }

    /**
     * @brief Swaps two matrices.
     * 
     * @param lhs First matrix.
     * @param rhs Second matrix.
     */
    void swap(Matrix& lhs, Matrix& rhs) noexcept
    {
        std::swap(lhs.rows_, rhs.rows_);
        std::swap(lhs.columns_, rhs.columns_);
        std::swap(lhs.area_, rhs.area_);
        std::swap(lhs.mat_, rhs.mat_);
    }
    
    /**
     * @brief Configures the neural network.
     * 
     * @param input_size How many variables the input layer contains.
     * @param hidden_layers How many hidden layers the network contains.
     * @param hidden_layer_size How many neurons in each hidden layer.
     * @param output_size How many neurons in the output.
     * @param learning_curve How fast the network learns.
     * @param learning_curve_halve_rate How often to halve the learning rate. 0 means no halving is         performed.
     */
    AIConfig::AIConfig(size_t input_size, size_t hidden_layers, size_t hidden_layer_size, 
                       size_t output_size, double learning_curve, size_t learning_curve_halve_rate)
                                                                            :
                                                                                input_size_{input_size},
                                                                                hidden_layers_{hidden_layers},
                                                                                hidden_layer_size_{hidden_layer_size},
                                                                                output_size_{output_size},
                                                                                learning_curve_{learning_curve},
                                                                                learning_curve_halve_rate_{learning_curve_halve_rate}
    {
        if (input_size == 0 || output_size == 0 || learning_curve == 0 || output_size > input_size)
            throw std::logic_error("Invalid parameters!");

        if (hidden_layers > 0)
            if (hidden_layer_size == 0 || hidden_layer_size > input_size || output_size > hidden_layer_size)
                throw std::logic_error("Invalid parameters!");
    }

    /**
     * @brief Create the layers, weights, biases, and activations that are specified by the configuration.
     * 
     * @param config Configuration.
     */
    AI::AI(const AIConfig& config) : config_{config}, learning_curve_{config.learning_curve_}, current_iterations_{0}
    {
        // reserve all memory alloctions in advance
        activations_.reserve(2 + config.hidden_layers_);
        biases_.reserve(1 + config.hidden_layers_);
        weights_.reserve(1 + config.hidden_layers_);

        activations_.emplace_back(Matrix{config_.input_size_, 1});

        if (config.hidden_layers_ > 0)
        {
            // insert initial weight (input -> hidden layer)
            weights_.emplace_back(Matrix{config.hidden_layer_size_, config_.input_size_});

            // insert biases, weights, and activations
            for (size_t i = 0; i < config.hidden_layers_; ++i)
            {
                activations_.emplace_back(Matrix{config.hidden_layer_size_, 1});
                biases_.emplace_back(Matrix{config.hidden_layer_size_, 1});

                if (i < (config.hidden_layers_ - 1))
                    weights_.emplace_back(Matrix{config.hidden_layer_size_, config.hidden_layer_size_});
            }

            weights_.emplace_back(Matrix{config.output_size_, config.hidden_layer_size_});
        }
        else
        {
            weights_.emplace_back(Matrix{config.output_size_, config.input_size_});
        }

        biases_.emplace_back(Matrix{config.output_size_, 1});
        activations_.emplace_back(Matrix{config_.output_size_, 1});

        // randomize weights and biases
        for (auto& i : weights_)
            Randomize(i);
        for (auto& i : biases_)
            Randomize(i);
    }

    /**
     * @brief Performs forward propagation on the network.
     * 
     * @param input Input matrix.
     */
    void AI::ForwardPropagate(const Matrix& input)
    {
        if (input.Rows() != config_.input_size_ || input.Columns() != 1)
            throw std::logic_error("Invalid matrix being set as input!");

        // set the input
        activations_[0] = input;

        for (size_t i = 1; i < activations_.size(); ++i)
        {
            {
                Matrix&& intermediary = weights_[i - 1] * activations_[i - 1];
                intermediary += biases_[i - 1];
                activations_[i] = std::move(intermediary);
            }

            for (size_t j = 0; j < activations_[i].Rows(); ++j)
                activations_[i][j][0] = (1 / (1 + std::exp(-activations_[i][j][0])));
        }
    }

    /**
     * @brief Perform backwards propagation to bring the neural network closer to the minimum cost.
     * 
     */
    void AI::BackwardPropagate(const Matrix& expected)
    {    
        AdjustLearningCurve();

        // get the error
        Matrix error = activations_.back() - expected;

        // create temporaries to write into
        std::vector<std::vector<double>> last_delta;
        std::vector<Matrix> updated_weights(weights_);

        // backwards propagate
        for (size_t i = 0; i < weights_.size(); ++i)
        {
            size_t outer_iterations = weights_[weights_.size() - 1 - i].Rows();
            std::vector<std::vector<double>> temp_delta;
            temp_delta.reserve(outer_iterations);

            for (size_t x = 0; x < outer_iterations; ++x)
            {
                // derivative of error
                double error_val = 0;

                if (i == 0)
                {
                    error_val = error[x][0];
                }
                else
                {
                    // sum up errors
                    for (const auto& vec : last_delta)
                        error_val += vec[x];
                }

                // the bias affects the derivative and conversely the error
                biases_[biases_.size() - 1 - i][x][0] -= error_val * learning_curve_;

                // vector to store previous Deltas in
                size_t inner_iter = weights_[weights_.size() - 1 - i].Columns();
                std::vector<double> temp;
                temp.reserve(inner_iter);

                // adjust weights
                for (size_t y = 0; y < inner_iter; ++y)
                {
                    double delta_weight = error_val * activations_[activations_.size() - 2 - i][y][0];

                    // rows = weights of all input nodes connected to me side by side
                    updated_weights[updated_weights.size() - 1 - i][x][y] -= delta_weight * learning_curve_;

                    // store vales for each
                    temp.emplace_back(error_val * weights_[weights_.size() - 1 - i][x][y]);
                }

                // push deltas to the next layer
                temp_delta.emplace_back(temp);
            }

            last_delta = temp_delta;
        }

        // modify all of the weights
        weights_ = updated_weights;
    }

    /**
     * @brief Returns the activation values of the output neurons.
     * 
     * @return const Matrix& 
     */
    const Matrix& AI::GetOutput() const noexcept
    {
        return activations_.back();
    }

    /**
     * @brief Gets the index of the neuron and its activation (max).
     * 
     * @return std::pair<size_t, double> 
     */
    std::pair<size_t, double> AI::GetMaxOutput() const noexcept
    {
        const Matrix& output = activations_.back();

        size_t max_index = 0;

        for (size_t i = 1; i < output.Rows(); ++i)
            if (output[i][0] > output[max_index][0])
                max_index = i;

        return {max_index, output[max_index][0]};
    }

    /**
     * @brief Gets the index of the neuron and its activation (min).
     * 
     * @return std::pair<size_t, double> 
     */
    std::pair<size_t, double> AI::GetMinOutput() const noexcept
    {
        const Matrix& output = activations_.back();

        size_t min_index = 0;

        for (size_t i = 1; i < output.Rows(); ++i)
            if (output[i][0] < output[min_index][0])
                min_index = i;

        return {min_index, output[min_index][0]};
    }

    /**
     * @brief Gets a vector containing all neurons with an activation above a certain threshold.
     * 
     * @param threshold Minimum activation for the neuron to be acceptable.
     * @return std::vector<size_t> 
     */
    std::vector<size_t> AI::GetThresholdNeurons(double threshold) const
    {
        const Matrix& output = activations_.back();
        
        std::vector<size_t> indices;
        indices.reserve(output.Rows());

        for (size_t i = 0; i < output.Rows(); ++i)
            if (output[i][0] >= threshold)
                indices.emplace_back(i);

        return indices;
    }

    /**
     * @brief Resets the current learning curve back to what was originally specified.
     * 
     */
    void AI::ResetLearningCurve() noexcept
    {
        learning_curve_ = config_.learning_curve_;
        current_iterations_ = 0;
    }

    /**
     * @brief Saves the current state of the network into a file.
     * 
     * @param file_name The file name to write to.
     */
    void AI::SaveState(const std::string& file_name) const
    {
        std::ofstream file;
        file.open(file_name);

        file.precision(16);

        // write to file
        file << "CONFIGURATION\n";

        file << "INPUT_SIZE: " << config_.input_size_ << '\n';
        file << "HIDDEN_LAYERS: " << config_.hidden_layers_ << '\n';
        file << "HIDDEN_LAYER_SIZE: " << config_.hidden_layer_size_ << '\n';
        file << "OUTPUT_SIZE: " << config_.output_size_ << '\n';
        file << "LEARNING_CURVE: " << config_.learning_curve_ << '\n';
        file << "LEARNING_CURVE_HALVE_RATE: " << config_.learning_curve_halve_rate_ << '\n';

        file << "\nBEGINWEIGHTS\n";

        for (auto it = std::cbegin(weights_); it < std::cend(weights_); ++it)
        {
            file << (*it) << '\n';

            if (it < std::cend(weights_) - 1)
                file << '\n';
        }
        
        file << "ENDWEIGHTS\n\n";
        file << "BEGINBIASES\n";

        // print out all the biases
        for (auto it = std::cbegin(biases_); it < std::cend(biases_); ++it)
        {
            for (size_t i = 0; i < it->Rows(); ++i)
            {         
                file << (*it)[i][0];

                if (i < it->Rows() - 1)
                    file << ' ';
            }

            file << '\n';

            if (it < std::cend(biases_) - 1)
                file << '\n';
        }

        file << "ENDBIASES\n\n";

        file << "BEGIN_CURR_LEARNING_CURVE\n";
        file << learning_curve_ << '\n';
        file << "END_CURR_LEARNING_CURVE\n\n";

        file << "BEGIN_CURR_ITERATIONS\n";
        file << current_iterations_ << '\n';
        file << "END_CURR_ITERATIONS";

        file.close();
    }

    /**
     * @brief Loads a saved network state from a file. Note: Assumes 
     *        the configuration of the current network matches the 
     *        configuraton of the saved network.
     * 
     * @param file_name The file name to load from.
     */
    void AI::LoadState(const std::string& file_name)
    {
        std::ifstream input(file_name);

        // make sure file is valid
        if (input.good())
        {
            std::string str;

            bool reading_weights = false;
            bool reading_biases = false;
            bool reading_curve = false;
            bool reading_iterations = false;

            // used for reading the weights in
            size_t row_index = 0;

            // used for reading the biases in
            size_t index = 0;

            while (std::getline(input, str))
            {
                // ignore blanks
                if (str.empty())
                {
                    if (reading_weights)
                    {
                        ++index;
                        row_index = 0;
                    }
                    else if (reading_biases)
                        ++index;

                    continue;
                }

                if (str == "BEGINWEIGHTS")
                {
                    index = 0;
                    reading_weights = true;
                    continue;
                }
                else if (str == "ENDWEIGHTS")
                {
                    reading_weights = false;
                    continue;
                }
                else if (str == "BEGINBIASES")
                {
                    index = 0;
                    reading_biases = true;
                    continue;
                }
                else if (str == "ENDBIASES")
                {
                    reading_biases = false;
                    continue;
                }
                else if (str == "BEGIN_CURR_LEARNING_CURVE")
                {
                    reading_curve = true;
                    continue;
                }
                else if (str == "END_CURR_LEARNING_CURVE")
                {
                    reading_curve = false;
                    continue;
                }
                else if (str == "BEGIN_CURR_ITERATIONS")
                {
                    reading_iterations = true;
                    continue;
                }
                else if (str == "END_CURR_ITERATIONS")
                {
                    reading_iterations = false;
                    continue;
                }

                // get the data on the line
                std::istringstream is(str);
                is.precision(16);

                std::vector<double> line_data ((std::istream_iterator<double>(is)), std::istream_iterator<double>());

                if (reading_weights)
                {
                    Matrix& current = weights_[index];

                    if (line_data.size() != current.Columns())
                        throw std::runtime_error("Invalid dimensions! Check file!");
                    
                    for (size_t i = 0; i < current.Columns(); ++i)
                        current[row_index][i] = line_data[i];

                    ++row_index;
                }
                else if (reading_biases)
                {
                    Matrix& current = biases_[index];

                    if (line_data.size() != current.Rows())
                        throw std::runtime_error("Invalid dimensions! Check file!");
                    
                    for (size_t i = 0; i < current.Rows(); ++i)
                        current[i][0] = line_data[i];
                }
                else if (reading_curve)
                {
                    if (line_data.size() != 1)
                        throw std::runtime_error("Learning curve not found!");
                    learning_curve_ = line_data[0];
                }
                else if (reading_iterations)
                {
                    if (line_data.size() != 1)
                        throw std::runtime_error("Learning curve iterations not found!");
                    current_iterations_ = line_data[0];
                }
            }
        }
    }

    /**
     * @brief Reduces learning curve over time if specified.
     * 
     */
    void AI::AdjustLearningCurve()
    {
        // reduces the learning curve over time
        if (config_.learning_curve_halve_rate_ > 0)
        {
            if (current_iterations_ == config_.learning_curve_halve_rate_)
            {
                learning_curve_ *= 0.5f;

                constexpr auto EPSILON = std::numeric_limits<double>::epsilon();

                // don't let learning curve drop below zero
                if (learning_curve_ > -EPSILON && learning_curve_ < EPSILON)
                    learning_curve_ = 0;
                    
                current_iterations_ = 0;
            }
            ++current_iterations_;
        }
    }
}
