/******************************************************************************/
/*
* @file   main.cpp
* @author Aditya Harsh
* @brief  Neural-network framework.

          Copyright (c) 2018 Aditya Harsh
          Permission is hereby granted, free of charge, to any person obtaining 
          a copy of this software and associated documentation files 
          (the "Software"), to deal in the Software without restriction, 
          including without limitation the rights to use, copy, modify, merge, 
          publish, distribute, sublicense, and/or sell copies of the Software, 
          and to permit persons to whom the Software is furnished to do so, 
          subject to the following conditions:
          The above copyright notice and this permission notice shall be 
          included in all copies or substantial portions of the Software.
          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
          MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
          NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
          BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
          ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
          CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
          SOFTWARE.
*/
/******************************************************************************/

#include "aichan.h"

#include <cmath>
#include <vector>
#include <iomanip>
#include <numeric>

using namespace AIChan;

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        std::cout << "Please enter the number of training sessions to give the AI...\n";
        return -1;
    }

    AI network{{3, 1, 3, 3, 0.2}};

    // formatting
    std::cout << std::fixed << std::setprecision(2);

    // All activations
    Matrix input_activations{3, 1}; // 3 moves

    // average tracking (all moves are equal in the beginning)
    size_t rock_rat = 1;
    size_t paper_rat = 1;
    size_t scissor_rat = 1;

    auto execute_game = 
    [&](size_t move)
    {
        // the target to strive towards
        Matrix target{3, 1};

        switch (move)
        {
        case 0:
            target[1][0] = 1; // paper
            break;
        case 1:
            target[2][0] = 1; // scissor
            break;
        case 2:
            target[0][0] = 1; // rock
            break;
        default:
            throw std::runtime_error("Invalid move");
        }

        // simplify
        size_t gcd = std::gcd(std::gcd(rock_rat, paper_rat), scissor_rat);
        rock_rat /= gcd;
        paper_rat /= gcd;
        scissor_rat /= gcd;

        // average out input
        size_t sum = rock_rat + paper_rat + scissor_rat;
        input_activations[0][0] = static_cast<double>(rock_rat) / sum;
        input_activations[1][0] = static_cast<double>(paper_rat) / sum;
        input_activations[2][0] = static_cast<double>(scissor_rat) / sum;

        network.ForwardPropagate(input_activations);     
        network.BackwardPropagate(target);

        // remember previous input
        switch (move)
        {
        case 0:
            ++rock_rat;
            break;
        case 1:
            ++paper_rat;
            break;
        case 2:
            ++scissor_rat;
            break;
        }
    };

    std::cout << "Training AI...\n";

    size_t iterations = static_cast<size_t>(std::atol(argv[1]));

    // play a bunch of random games against itself
    for (size_t i = 0; i < iterations; ++i)
        execute_game(i % 3);

    auto help_print = []
    {
        // show user possible interactions
        std::cout << "Commands:\n";
        std::cout << "- Rock\n";
        std::cout << "- Paper\n";
        std::cout << "- Scissors\n";
        std::cout << "- Save\n";
        std::cout << "- Load\n";
        std::cout << "- Help\n";
        std::cout << "- Exit\n\n";
    };

    help_print();

    // play with the user
    std::string input;
    
    while (1)
    {
        std::cin >> input;

        for (auto& i : input)
            i = std::tolower(i);

        if (input == "rock")
        {
            execute_game(0);
        }
        else if (input == "paper")
        {
            execute_game(1);
        }
        else if (input == "scissors" || input == "scissor")
        {
            execute_game(2);
        }
        else if (input == "exit" || input == "quit")
        {
            break;
        }
        else if (input == "save")
        {
            std::string fn;
            std::cout << "Enter filename:\n";
            std::cin >> fn;
            network.SaveState(fn);
            std::cout << "State saved.\n";
            continue;
        }
        else if (input == "load")
        {
            std::string fn;
            std::cout << "Enter filename:\n";
            std::cin >> fn;
            network.LoadState(fn);
            std::cout << "State loaded.\n";
            continue;
        }
        else if (input == "help")
        {
            help_print();
            continue;
        }
        else
        {
            std::cout << "Invalid input!\n";
            continue;
        }

        // print results
        std::cout << "Computer played: ";

        Matrix output = network.GetOutput();

        float rock = output[0][0];
        float paper = output[1][0];
        float scissors = output[2][0];

        if (rock > paper)
        {
            if (rock > scissors)
            {
                std::cout << "Rock\n";
            }
            else
            {
                std::cout << "Scissors\n";
            }
        }
        else if (paper > scissors)
        {
            std::cout << "Paper\n";
        }
        else
        {
            std::cout << "Scissors\n";
        }

        // print out meta-data
        std::cout << "\nNeuron Activation:\n";
        std::cout << "- Rock: " << (100 * rock) << '%' << '\n';
        std::cout << "- Paper: " << (100 * paper) << '%' << '\n';
        std::cout << "- Scissors: " << (100 * scissors) << '%' << '\n' << '\n';
    }
    
    // clean exit
    return 0;
}