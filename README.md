# AI Chan (アイちゃん) #

AI is a neural network framework written in C++ for solving new problems given adequate amounts of training data. For example, you can use AI for: image recognition, games, any sort of pattern recognition, etc.

Using AI is extremely easy. The program consists of only two files: aichan.cpp and aichan.h.

If you need more information of how neural networks work, consider reading up on [this](https://www.gamedev.net/articles/programming/artificial-intelligence/neural-networks-101-r4870/) article.

十分なトレーニングデータがあれば、アイちゃんは新しい問題を解けるC＋＋フレイムワークです。
例えば、画像認識、ゲームトレーニング、そしてパターン認識のために使います。

アイを使うのは、非常に簡単です。このプログラムは、二つのファイルだけで出来ています：aichan.cppとaichan.h.

もしニューラルネットワークについてもっと情報が必要なら、この[記事](https://www.gamedev.net/articles/programming/artificial-intelligence/neural-networks-101-r4870/)を読んで下さい。

### Prerequisites
```
AI Chan requires a C++14 or higher compliant compiler.
Note: The example will only compile on a C++17 compliant compiler since it uses std::gcd.
```

### Who do I talk to? ###

* Aditya Harsh
* mrsaturnsan@gmail.com
* Twitter: @adityaXharsh

# Usage #
```
using namespace AIChan;

AI network{...};

// pass input to AI
network.ForwardPropagate(INPUT);

// evaluate AI performance
network.BackwardPropagate(EXPECTED);

// get the training result
Matrix output = network.GetOutput();
```

# Working Example: Rock, Paper, Scissors #

```
    Adityas-MacBook-Pro:Binary adityaharsh$ ./ai.out 999999
    Training AI...
    Commands:
    - Rock
    - Paper
    - Scissors
    - Exit

    rock
    Computer played: Paper

    Neuron Activation:
    - Rock: 2.52%
    - Paper: 96.82%
    - Scissors: 2.58%

    paper
    Computer played: Scissors

    Neuron Activation:
    - Rock: 1.74%
    - Paper: 1.88%
    - Scissors: 97.20%

    scissor
    Computer played: Rock

    Neuron Activation:
    - Rock: 97.04%
    - Paper: 2.39%
    - Scissors: 1.69%
```

Here is a section of the example on training AI to play "Rocks, Paper, Scissors."
You can find the full version in main.cpp.

```
    AI network{{3, 1, 3, 3, 0.001, [](double x) {return 1 / (1 + std::exp(-x));}, [](double x) {return x * (1 - x);}}};

    // All activations
    Matrix input_activations{3, 1}; // 3 moves

    // average tracking (all moves are equal in the beginning)
    size_t rock_rat = 1;
    size_t paper_rat = 1;
    size_t scissor_rat = 1;

    auto execute_game = 
    [&](size_t move)
    {
        // the target to strive towards
        Matrix target{3, 1};

        switch (move)
        {
        case 0:
            target[1][0] = 1; // paper
            break;
        case 1:
            target[2][0] = 1; // scissor
            break;
        case 2:
            target[0][0] = 1; // rock
            break;
        default:
            throw std::runtime_error("Invalid move");
        }

        // simplify
        size_t gcd = std::gcd(std::gcd(rock_rat, paper_rat), scissor_rat);
        rock_rat /= gcd;
        paper_rat /= gcd;
        scissor_rat /= gcd;

        // average out input
        size_t sum = rock_rat + paper_rat + scissor_rat;
        input_activations[0][0] = static_cast<float>(rock_rat) / sum;
        input_activations[1][0] = static_cast<float>(paper_rat) / sum;
        input_activations[2][0] = static_cast<float>(scissor_rat) / sum;

        network.ForwardPropagate(input_activations);
        network.BackwardPropagate(target);

        // remember previous input
        switch (move)
        {
        case 0:
            ++rock_rat;
            break;
        case 1:
            ++paper_rat;
            break;
        case 2:
            ++scissor_rat;
            break;
        }
    };

    // play a bunch of random games against itself
    for (size_t i = 0; i < static_cast<size_t>(std::atol(argv[1])); ++i)
        execute_game(i % 3);
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments

* Inspired by 3Blue1Brown's great explanation of machine learning.